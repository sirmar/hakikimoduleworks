#include "my3dwindow.h"
#include <QDebug>
#include <QKeyEvent>

My3DWindow::My3DWindow(QScreen *screen):Qt3DExtras::Qt3DWindow(screen)
{
    root_entity_ = new Qt3DCore::QEntity;

    // Material
    Qt3DRender::QMaterial *material = new Qt3DExtras::QPhongMaterial(root_entity_);

    // Torus
    tool_entity_ = new Qt3DCore::QEntity(root_entity_);

    QUrl data = QUrl::fromLocalFile("C:/Users/z003z2eu/Downloads/Mesh_5.stl");
    tool_mesh_ = new Qt3DRender::QMesh;
    tool_mesh_->setMeshName("Tool Mesh");
    tool_mesh_->setSource(data);

    tool_transform_ = new Qt3DCore::QTransform;
    tool_transform_->setScale3D(QVector3D(0.25, 0.25, 0.25));
    tool_transform_->setRotation(QQuaternion::fromAxisAndAngle(QVector3D(1, 0, 0), 270.0f));
    tool_transform_->setTranslation(QVector3D(0, 10, 0));

    tool_entity_->addComponent(tool_mesh_);
    tool_entity_->addComponent(tool_transform_);
    tool_entity_->addComponent(material);

    // Sphere
    Qt3DCore::QEntity *sphereEntity = new Qt3DCore::QEntity(root_entity_);
    Qt3DExtras::QSphereMesh *sphereMesh = new Qt3DExtras::QSphereMesh;
    sphereMesh->setRadius(3);

    Qt3DCore::QTransform *sphereTransform = new Qt3DCore::QTransform;

    sphereEntity->addComponent(sphereMesh);
    sphereEntity->addComponent(sphereTransform);
    sphereEntity->addComponent(material);

    // Camera
    Qt3DRender::QCamera *camera = this->camera();
    camera->setPosition(QVector3D(0, 0, 40.0f));
    camera->setViewCenter(QVector3D(0, 0, 0));

    // For camera controls
    Qt3DExtras::QOrbitCameraController *camController = new Qt3DExtras::QOrbitCameraController(root_entity_);
    camController->setLinearSpeed( 50.0f );
    camController->setLookSpeed( 180.0f );
    camController->setCamera(camera);

    setRootEntity(root_entity_);
}

My3DWindow::~My3DWindow()
{

}

void My3DWindow::keyPressEvent(QKeyEvent *ev)
{
    QVector3D translation = tool_transform_->translation();

    switch (ev->key()) {
    case Qt::Key_W:
        qDebug()<<"Key_W is pressed";
        translation.setY(translation.y() + 0.1f);
        break;
    case Qt::Key_A:
        qDebug()<<"Key_A is pressed";
        translation.setX(translation.x() - 0.1f);
        break;
    case Qt::Key_S:
        qDebug()<<"Key_S is pressed";
        translation.setY(translation.y() - 0.1f);
        break;
    case Qt::Key_D:
        qDebug()<<"Key_D is pressed";
        translation.setX(translation.x() + 0.1f);
        break;
    default:
        break;
    }

    tool_transform_->setTranslation(translation);
}
