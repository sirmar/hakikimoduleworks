QT += 3dcore 3drender 3dinput 3dextras

SOURCES += \
    main.cpp \
    my3dwindow.cpp \
    orbittransformcontroller.cpp

HEADERS += \
    my3dwindow.h \
    orbittransformcontroller.h
