#pragma once
#include <QObject>
#include <QMatrix4x4>
#include <QGuiApplication>

#include <Qt3DCore/QEntity>
#include <Qt3DRender/QCamera>
#include <Qt3DRender/QCameraLens>
#include <Qt3DCore/QTransform>
#include <Qt3DCore/QAspectEngine>

#include <Qt3DInput/QInputAspect>
#include <Qt3DInput/QKeyEvent>
#include <Qt3DRender/QRenderAspect>
#include <Qt3DExtras/QForwardRenderer>
#include <Qt3DExtras/QPhongMaterial>
#include <Qt3DExtras/QCylinderMesh>
#include <Qt3DExtras/QSphereMesh>
#include <Qt3DExtras/QTorusMesh>
#include <QMesh>

#include <QPropertyAnimation>

#include "qt3dwindow.h"
#include "qorbitcameracontroller.h"

QT_BEGIN_NAMESPACE

namespace Qt3DCore {
class QTransform;
}

class MainWindow : public Qt3DExtras::Qt3DWindow
{
    Q_OBJECT

public:
    MainWindow(QObject *parent = nullptr);

};

QT_END_NAMESPACE
